package it.facile.demoFacile.utils;

import java.util.ArrayList;
import java.util.HashMap;

import it.facile.demoFacile.dto.RedditDto;
import it.facile.demoFacile.dto.RootObjectJsonDto;
import it.facile.demoFacile.dto.SubRedditDto;

public class TestUtils {

	public static RootObjectJsonDto createMockRootObjectJsonDtoResponse(){
		RootObjectJsonDto rojd = new RootObjectJsonDto();
		rojd.setKind("Listening");
		RedditDto redditDto = new RedditDto();
		redditDto.setDist(2);
		ArrayList<SubRedditDto> subReddits = new ArrayList<SubRedditDto>();
		HashMap<String, Object> firstMap = createHashMap("IT","www.google.it/1");
		HashMap<String,Object> secondMap = createHashMap("Pulp Fiction", "www.google/2");
		subReddits.add(createSubReddit(firstMap));
		subReddits.add(createSubReddit(secondMap));
		redditDto.setSubReddit(subReddits);
		rojd.setData(redditDto);
		return rojd;
		
	}

	private static HashMap<String, Object> createHashMap(String title, String thumbnail) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("title", title);
		hashMap.put("thumbnail", thumbnail);
		return hashMap;
	}

	private static SubRedditDto createSubReddit(HashMap<String, Object> data) {
		SubRedditDto sub = new SubRedditDto();
		sub.setData(data);
		sub.setKind("t3");
		return sub;
	}

}
