package it.facile.demoFacile.service;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import it.facile.demoFacile.dao.RedditOutputDao;
import it.facile.demoFacile.repository.RedditOutputRepository;
import junit.framework.Assert;


@RunWith(MockitoJUnitRunner.class)
public class RedditOutputServiceTest {
	
	@Mock
	private RedditOutputRepository redditOutputRepository;
	
	@InjectMocks
	private RedditOutputService  redditOutputService;

	@Test
	public void testService(){
		RedditOutputDao rod = new RedditOutputDao();
		rod.setTitle("Hannibal");
		List<RedditOutputDao> reddits = new ArrayList<RedditOutputDao>();
		reddits.add(rod);
		Mockito.when(redditOutputRepository.save(rod)).thenReturn(rod);
		List<RedditOutputDao> rodDB = redditOutputService.saveListRedditOutputDao(reddits);
		Assert.assertEquals("Hannibal", rodDB.get(0).getTitle());
	}
	
}
