package it.facile.demoFacile.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.facile.demoFacile.dto.RootObjectJsonDto;
import it.facile.demoFacile.utils.TestUtils;

@RunWith(MockitoJUnitRunner.class)
public class ReadFromUrlServiceTest {

	@Mock
	private RestTemplate rest;
	
	@Mock
	private ObjectMapper objectMapper;
	
	@InjectMocks
	private ReaderFromUrlService readerFromUrlService;
	
	@Test
	public void testService() throws Exception{
		Mockito.when(rest.getForEntity(Matchers.anyString(),  Matchers.<Class<RootObjectJsonDto>>any())).thenReturn(new ResponseEntity<RootObjectJsonDto>(TestUtils.createMockRootObjectJsonDtoResponse(), HttpStatus.ACCEPTED));
		RootObjectJsonDto responseJson = readerFromUrlService.readJsonFromUrl("www.google.com/example.json");
		Assert.assertNotNull(responseJson);
	}
	
}
