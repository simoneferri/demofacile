package it.facile.demoFacile.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import it.facile.demoFacile.dao.RedditOutputDao;
import it.facile.demoFacile.dto.RedditDto;
import it.facile.demoFacile.dto.RedditOutputDto;
import it.facile.demoFacile.dto.RootObjectJsonDto;
import it.facile.demoFacile.dto.SubRedditDto;
import it.facile.demoFacile.service.ReaderFromUrlService;
import it.facile.demoFacile.service.RedditOutputService;
import it.facile.demoFacile.utils.TestUtils;
import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class ApiControllerTest {
	
	@Mock
	private ReaderFromUrlService readerFromUrlService;
	
	@Mock
	private RedditOutputService redditOutputService;
	
	@InjectMocks
	private ApiController apiController;
	
	@Test
	public void controllerTest() throws Exception{	
		Mockito.when(readerFromUrlService.readJsonFromUrl("https://www.reddit.com/r/todayilearned/top.json")).thenReturn(TestUtils.createMockRootObjectJsonDtoResponse());
		//Mockito.when(redditOutputService.saveListRedditOutputDao(new ArrayList<RedditOutputDao>())).thenReturn(new ArrayList<RedditOutputDao>());
		ResponseEntity<ArrayList<RedditOutputDto>> redditOutput = apiController.getRedditOutputDto(new HttpHeaders(), "title,thumbnail", 1);
		Assert.assertEquals("IT" ,redditOutput.getBody().get(0).getTitle());
		Assert.assertEquals("www.google.it/1" ,redditOutput.getBody().get(0).getThumbnail());
		Assert.assertEquals(1, redditOutput.getBody().size());
	}
	
	@Test
	public void controllerTestFailed() throws Exception{	
		Mockito.when(readerFromUrlService.readJsonFromUrl("https://www.reddit.com/r/todayilearned/top.json")).thenReturn(TestUtils.createMockRootObjectJsonDtoResponse());
		ResponseEntity<ArrayList<RedditOutputDto>> response = apiController.getRedditOutputDto(new HttpHeaders(), "title,thumbnail", 4);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode().BAD_REQUEST);
	}
}
