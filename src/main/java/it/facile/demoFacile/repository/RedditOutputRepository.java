package it.facile.demoFacile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.facile.demoFacile.dao.RedditOutputDao;

@Repository
public interface RedditOutputRepository extends JpaRepository<RedditOutputDao, Long> {

}


	

