package it.facile.demoFacile.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.facile.demoFacile.dto.RootObjectJsonDto;

@Service
public class ReaderFromUrlService {

	private static final Logger logger = LoggerFactory.getLogger(ReaderFromUrlService.class);
	
	@Autowired
	private RestTemplate rest;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	public RootObjectJsonDto readJsonFromUrl(String urlToCall) throws Exception{
		logger.info("Call to URL " + urlToCall);
		ResponseEntity<RootObjectJsonDto> response = rest.getForEntity(urlToCall, RootObjectJsonDto.class);
		RootObjectJsonDto responseBody = response.getBody();
		logger.info("response: " + objectMapper.writeValueAsString(responseBody));
		return responseBody;
	}
	

}
