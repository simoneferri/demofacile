package it.facile.demoFacile.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.facile.demoFacile.dao.RedditOutputDao;
import it.facile.demoFacile.repository.RedditOutputRepository;

@Service
public class RedditOutputService {

	@Autowired
	private RedditOutputRepository redditOutputRepository;
	
	public List<RedditOutputDao> getAllRedditOutputRepository(){
		return redditOutputRepository.findAll();
	}
	
	public RedditOutputDao saveRedditOutputDao(RedditOutputDao rod){
		return redditOutputRepository.save(rod);
	}
	
	public List<RedditOutputDao> saveListRedditOutputDao(List<RedditOutputDao> reddits){
		List<RedditOutputDao> redditsDB = new ArrayList<RedditOutputDao>();
		for(RedditOutputDao rdo : reddits){
			RedditOutputDao rdoDB = redditOutputRepository.save(rdo);
			redditsDB.add(rdoDB);
		}
		return redditsDB;
		
	}
	
	
	
	
}
