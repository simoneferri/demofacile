package it.facile.demoFacile.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


public class RootObjectJsonDto {

	@JsonProperty("kind")
	private String kind;
	
	@JsonProperty("data")
	private RedditDto data;
	
	public String getKind() {
		return kind;
	}
	
	public void setKind(String kind) {
		this.kind = kind;
	}
	
	public RedditDto getData() {
		return data;
	}
	
	public void setData(RedditDto data) {
		this.data = data;
	}
	
	
}
