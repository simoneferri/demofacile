package it.facile.demoFacile.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RedditOutputDto {
	
	@JsonProperty("title")
	private String title;
	
	@JsonProperty("thumbnail_width")
	private Integer thumbnailWidth;
	
	@JsonProperty("saved")
	private Boolean saved;
	
	@JsonProperty("subreddit_type")
	private String subredditType;
	
	@JsonProperty("thumbnail")
	private String thumbnail;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getThumbnailWidth() {
		return thumbnailWidth;
	}

	public void setThumbnailWidth(Integer thumbnailWidth) {
		this.thumbnailWidth = thumbnailWidth;
	}

	public Boolean getSaved() {
		return saved;
	}

	public void setSaved(Boolean saved) {
		this.saved = saved;
	}

	public String getSubredditType() {
		return subredditType;
	}

	public void setSubredditType(String subredditType) {
		this.subredditType = subredditType;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	

}
