package it.facile.demoFacile.dto;

import java.util.HashMap;

/*
 * 
 * Per semplicità ho utilizzato una mappa chiave valore.
 * Avendo più tempo avrei costruito il Dto "corretto" dell'oggetto finale.
 * 
 * */
public class SubRedditInfoDto {

	HashMap<String,Object> mapKeyValue;
	

	public HashMap<String, Object> getMapKeyValue() {
		return mapKeyValue;
	}

	public void setMapKeyValue(HashMap<String, Object> mapKeyValue) {
		this.mapKeyValue = mapKeyValue;
	}

}
