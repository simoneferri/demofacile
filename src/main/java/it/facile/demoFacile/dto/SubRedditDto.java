package it.facile.demoFacile.dto;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubRedditDto {
	
	@JsonProperty("kind")
	private String kind;
	
	@JsonProperty("data")
	private HashMap<String,Object> data;
	
	
	//@JsonProperty("data")
	//private SubRedditInfoDto subRedditInfo;

	public HashMap<String, Object> getData() {
		return data;
	}

	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}
/*
	public SubRedditInfoDto getSubRedditInfo() {
		return subRedditInfo;
	}

	public void setSubRedditInfo(SubRedditInfoDto subRedditInfo) {
		this.subRedditInfo = subRedditInfo;
	}
*/
}
