package it.facile.demoFacile.dto;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RedditDto {
	
	@JsonProperty("modhash")
	private String modhash;

	@JsonProperty("dist")
	private int dist;

	@JsonProperty("children")
	private ArrayList<SubRedditDto> subReddit;

	@JsonProperty("after")
	private String after;
	
	@JsonProperty("before")
	private String before;

	public String getModhash() {
		return modhash;
	}

	public void setModhash(String modhash) {
		this.modhash = modhash;
	}

	public int getDist() {
		return dist;
	}

	public void setDist(int dist) {
		this.dist = dist;
	}

	public ArrayList<SubRedditDto> getSubReddit() {
		return subReddit;
	}

	public void setSubReddit(ArrayList<SubRedditDto> subReddit) {
		this.subReddit = subReddit;
	}

	public String getAfter() {
		return after;
	}

	public void setAfter(String after) {
		this.after = after;
	}

	public String getBefore() {
		return before;
	}

	public void setBefore(String before) {
		this.before = before;
	}
	
}
