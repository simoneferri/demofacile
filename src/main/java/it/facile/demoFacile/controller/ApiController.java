package it.facile.demoFacile.controller;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.facile.demoFacile.dao.RedditOutputDao;
import it.facile.demoFacile.dto.RedditOutputDto;
import it.facile.demoFacile.dto.RootObjectJsonDto;
import it.facile.demoFacile.dto.SubRedditDto;
import it.facile.demoFacile.dto.SubRedditInfoDto;
import it.facile.demoFacile.service.ReaderFromUrlService;
import it.facile.demoFacile.service.RedditOutputService;
import it.facile.demoFacile.utils.mapper.RedditOutputFromDtoToDBMapper;
import it.facile.demoFacile.utils.mapper.RootObjectJsonMapper;


@RestController
public class ApiController {
	
	private static final Logger logger = LoggerFactory.getLogger(ApiController.class);
	
	@Autowired
	private ReaderFromUrlService readerFromUrlService;
	
	@Autowired
	private RedditOutputService redditOutputService;
	
	@RequestMapping(value = "/redditOutput", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<RedditOutputDto>> getRedditOutputDto(@RequestHeader HttpHeaders headers,
										@RequestParam(value = "searchField", required = false) String searchField,
	                            		@RequestParam(value = "size", required = false) Integer size
	                                ) throws Exception {

		RootObjectJsonDto response = readerFromUrlService.readJsonFromUrl("https://www.reddit.com/r/todayilearned/top.json");
	
		logger.info("Filtered the results by {} size", size);
		Boolean isSizeMoreThanNumberElement = isSizeMoreThanNumberElement(response, size);
		
		if(isSizeMoreThanNumberElement){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		ArrayList<SubRedditDto> subReddits = subRedditDtoSizeFiltered(response, size);
		
		logger.info("Start mapping from Json to Java POJO object");
		ArrayList<RedditOutputDto> resultMapping = RootObjectJsonMapper.fromRootObjectJsonDtoToSubRedditInfoDto(subReddits, searchField);
		logger.info("End mapping");
		logger.info("Start mapping from Java POJO to DB");
		List<RedditOutputDao> resultMappingToDB = RedditOutputFromDtoToDBMapper.redditOutputFromDtoToDB(resultMapping);
		
		redditOutputService.saveListRedditOutputDao(resultMappingToDB);
		logger.info("End insert DB");
		return new ResponseEntity<ArrayList<RedditOutputDto>>(resultMapping, headers, HttpStatus.OK);

	}
	
	private Boolean isSizeMoreThanNumberElement(RootObjectJsonDto response, Integer size) throws Exception {
		Boolean isSizeMoreThanNumberElement = false;
		if(response != null && response.getData() != null && size != null && response.getData().getDist() < size){
			logger.error("Size is more than the result");
			isSizeMoreThanNumberElement = true;
		}
		return isSizeMoreThanNumberElement;
	}

	public static ArrayList<SubRedditDto> subRedditDtoSizeFiltered(RootObjectJsonDto rootObjectJsonDto, Integer size){
		ArrayList<SubRedditDto> subReddit = rootObjectJsonDto.getData().getSubReddit();
		if(size != null && size > 0){
			subReddit = new ArrayList<>(subReddit.subList(0, size));
		}
		return subReddit;
	}
	
	
}
