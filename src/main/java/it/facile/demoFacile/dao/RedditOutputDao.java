package it.facile.demoFacile.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "REDDIT_OUTPUT")
public class RedditOutputDao {

	@Id
    @GeneratedValue
    private Long id;
	
	@Column(length=1024)
	private String title;
	
	private Integer thumbnailWidth;
	
	private Boolean saved;
	
	private String subredditType;
	
	private String thumbnail;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getThumbnailWidth() {
		return thumbnailWidth;
	}

	public void setThumbnailWidth(Integer thumbnailWidth) {
		this.thumbnailWidth = thumbnailWidth;
	}

	public Boolean getSaved() {
		return saved;
	}

	public void setSaved(Boolean saved) {
		this.saved = saved;
	}

	public String getSubredditType() {
		return subredditType;
	}

	public void setSubredditType(String subredditType) {
		this.subredditType = subredditType;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	

}
