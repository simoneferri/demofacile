package it.facile.demoFacile.utils.mapper;

import java.util.ArrayList;
import java.util.List;

import it.facile.demoFacile.dao.RedditOutputDao;
import it.facile.demoFacile.dto.RedditOutputDto;

public class RedditOutputFromDtoToDBMapper {

	
	public static List<RedditOutputDao> redditOutputFromDtoToDB(ArrayList<RedditOutputDto> redditsDto){
		List<RedditOutputDao> redditsDao = new ArrayList<RedditOutputDao>();
		for(RedditOutputDto rod : redditsDto){
			redditsDao.add(mapperSingleRODDtoToDao(rod));
		}
		return redditsDao;
		
	}
	
	private static RedditOutputDao mapperSingleRODDtoToDao(RedditOutputDto rod){
		RedditOutputDao redditDao = new RedditOutputDao();
		redditDao.setSaved(rod.getSaved());
		redditDao.setSubredditType(rod.getSubredditType());
		redditDao.setThumbnail(rod.getThumbnail());
		redditDao.setThumbnailWidth(rod.getThumbnailWidth());
		redditDao.setTitle(rod.getTitle());
		return redditDao;
	}
	
}
