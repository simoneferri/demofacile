package it.facile.demoFacile.utils.mapper;

import java.lang.reflect.Field;
import java.util.ArrayList;

import it.facile.demoFacile.dto.RedditOutputDto;
import it.facile.demoFacile.dto.SubRedditDto;

public class RootObjectJsonMapper {

	public static ArrayList<RedditOutputDto> fromRootObjectJsonDtoToSubRedditInfoDto(ArrayList<SubRedditDto> subRedditsDto, String searchField) throws Exception{
		ArrayList<RedditOutputDto> redditOutputDto = new ArrayList<RedditOutputDto>(); 
		
		String[] searchFields = searchField.split(",");
		
		for(SubRedditDto subRedditDto : subRedditsDto){
			RedditOutputDto rod = createRedditOutputDto(subRedditDto, searchFields);
			redditOutputDto.add(rod);
		}
		
		return redditOutputDto;
	}

	private static RedditOutputDto createRedditOutputDto(SubRedditDto subRedditDto, String[] searchFields) throws Exception{
		RedditOutputDto rod = new RedditOutputDto();
		for(String filter : searchFields){
			if(subRedditDto.getData().containsKey(filter)){
				String value = (String) subRedditDto.getData().get(filter);
				Field field = rod.getClass().getDeclaredField(filter);
				field.setAccessible(true);
				field.set(rod, value);
			}
		}
		System.out.println(rod.getTitle());
		System.out.println(rod.getThumbnail());
		return rod;
	}
	
}
